import messaging from "@react-native-firebase/messaging";
import { Platform } from "react-native";

class FCMService {
  register = (onRegister, onNotification, onOpenNotification) => {
    this.checkPermission(onRegister);
    this.createNotificationListeners(
      onRegister,
      onNotification,
      onOpenNotification
    );
  };
  registerAppWithFCM = async () => {
    if (Platform.OS === "ios") {
      await messaging().registerDeviceForRemoteMessages();
      await messaging().setAutoInitEnabled(true);
    }
  };

  checkPermission = (onRegister) => {
    messaging()
      .hasPermission()
      .then((enabled) => {
        if (enabled) {
          //user has permission
          this.getToken(onRegister);
        } else {
          //user doesn't has permission
          this.requestPermission(onRegister);
        }
      })
      .catch((error) => {
        console.log("Error FCM", error);
      });
  };
  getToken = (onRegister) => {
    messaging()
      .getToken()
      .then((fcmToken) => {
        if (fcmToken) {
          onRegister(fcmToken);
        } else {
          console.log("user dont have fcm token");
        }
      })
      .catch((error) => {
        console.log("get fcmToken error", error);
      });
  };
  requestPermission = (onRegister) => {
    messaging()
      .requestPermission()
      .then(() => {
        this.getToken(onRegister);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  deleteToken = () => {
    messaging()
      .deleteToken()
      .catch((error) => {
        console.log("can not delete token", error);
      });
  };
  createNotificationListeners = (
    onRegister,
    onNotification,
    onOpenNotification
  ) => {
    //background
    messaging().onNotificationOpenedApp((remoteMessage) => {
      console.log("vooo");
      if (remoteMessage) {
        const notification = remoteMessage.notification;
        onOpenNotification(notification);
      }
    });
    //foreground message
    this.messageListener = messaging().onMessage(async (remoteMessage) => {
      if (remoteMessage) {
        let notification = null;
        if (Platform.OS === "ios") {
          notification = remoteMessage.data.notification;
        } else {
          notification = remoteMessage.notification;
        }
        onNotification(notification);
      }
    });
    //trigger when have new token
    messaging().onTokenRefresh((fcmToken) => {
      onRegister(fcmToken);
    });
  };
  unRegister = () => {
    this.messageListener();
  };
}
export const fcmService = new FCMService();

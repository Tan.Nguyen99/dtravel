export const fontFamily = {
  bold: "Arial",
  italic: "Arial",
  light: "Arial",
  regular: "Arialr",
  medium: "Arial",
  thin: "Arial",
};

export const fontWeight = {
  thin: "100", // Thin
  utraLight: "200", // Ultra Light
  light: "300", // Light
  regular: "400", // Regular
  medium: "500", // Medium
  semibold: "600", // Semibold
  bold: "bold", // Bold
  heavy: "800", // Heavy
  black: "900", // Black
  normal: "normal", // normal
};

export const fontSize = {
  tiny: 10,
  small: 12,
  medium: 14,
  xMedium: 16,
  large: 18,
  xLarge: 20,
  subtitle: 22,
  title: 24,
  xTitle: 26,
  header: 30,
};
export const type = {
  regular: "SFProDisplay-Regular",
  medium: "SFProDisplay-Medium",
  semibold: "SFProDisplay-Semibold",
  light: "SFProDisplay-Light",
};

export const size = {
  s8: 8,
  s10: 11,
  s12: 12,
  s14: 13,
  s16: 16,
  s17: 17,
  s18: 18,
  s20: 20,
  s24: 24,
  s28: 28,
  s30: 30,
  s36: 36,
  s40: 40,
};

export const appSize = {
  h1: 30,
  h2: 21,
  h3: 18,
  h4: 14,
  s16: 16,
  s14: 13,
  s12: 12,
  s10: 10,
  s8: 8,
};

export default { fontFamily, fontWeight, fontSize, type, appSize, size };

import React, { Component, useEffect } from "react";
import {
  Text,
  View,
  Image,
  TextInput,
  StyleSheet,
  TouchableWithoutFeedback,
  ScrollView,
  FlatList,
  Dimensions,
  ImageBackground,
  Alert,
} from "react-native";
import LoginActions from "../../redux/AuthRedux/actions";
import colors from "../../themes/Colors";
import { Container } from "../../components";
import metrics from "../../themes/Metrics";
import { Colors } from "../../themes";
import Icon from "react-native-vector-icons/Ionicons";
import { useSelector, useDispatch } from "react-redux";
import { fcmService } from "../../utils/FCMService";
import { localNotificationService } from "../../utils/LocalNotificationService";
import { NavigationUtils } from "../../navigation";
import messaging from "@react-native-firebase/messaging";
import firebase from "@react-native-firebase/app";
import Geolocation from "@react-native-community/geolocation";
import RNAndroidLocationEnabler from "react-native-android-location-enabler";
import AppAction from "../../redux/AppRedux/actions";
const { height, width } = Dimensions.get("window");

const Profile = () => {
  const dispatch = useDispatch();

  const onLogout = async () => {
    await dispatch(LoginActions.logout());
  };
  useEffect(() => {
    fcmService.registerAppWithFCM();
    fcmService.register(onRegister, onNotification, onOpenNotification);
    localNotificationService.configure(onOpenNotification);
    function onRegister(token) {
      console.log("registerToken", token);
    }
    function onNotification(notify) {
      console.log("onNotification", notify);
      const options = {
        soundName: "default",
        playSound: true,
      };
      localNotificationService.showNotification(
        0,
        notify.title,
        notify.body,
        notify,
        options
      );
    }

    function onOpenNotification(notify) {
      console.log("onOpenNotification", notify);
      Alert.alert("open notification", notify.body);
      NavigationUtils.push({ screen: "Cart" });
    }
    return () => {
      console.log("unRegister");
      fcmService.unRegister();
      localNotificationService.unRegister;
    };
  }, []);

  useEffect(() => {
    requestLocationPermission();
  }, []);
  const requestLocationPermission = async () => {
    if (Platform.OS === "ios") {
      var response = await request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE);
      if (response === "granted") {
        locationCurrentPosition();
      }
    } else {
      var response = await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
      if (response === "granted") {
        getPermissions();
      }
    }
  };

  const locationCurrentPosition = () => {
    Geolocation.getCurrentPosition(
      async (position) => {
        console.log("vooooo", position);
        let region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        };
        // setState({ initialRegion: region });
        // setCurrentLocation(region);
        await dispatch(AppAction.getCurrentLocation(region));
      },
      (error) => Alert.alert(error.message),
      {
        enableHighAccuracy: false,
        timeout: 10000,
      }
    );
  };

  const getPermissions = () => {
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
      interval: 10000,
      fastInterval: 5000,
    }).then((data) => {
      if (data === "already-enabled") {
        locationCurrentPosition();
      } else {
        setTimeout(() => {
          locationCurrentPosition();
        }, 1000);
      }
    });
  };

  return (
    <Container>
      <Image
        source={require("../../assets/img/userBackground.jpg")}
        style={{
          width: metrics.screenWidth,
          height: metrics.screenHeight / 3 - 24,
          backgroundColor: "white",
          position: "absolute",
          opacity: 0.4,
          borderBottomLeftRadius: 32,
          borderBottomRightRadius: 32,
        }}
      />
      <View
        style={{
          flexDirection: "row",
          justifyContent: "flex-end",
        }}
      >
        <TouchableWithoutFeedback
          onPress={() => {
            onLogout();
          }}
        >
          <View
            style={{
              width: 36,
              height: 36,
              backgroundColor: Colors.white,
              alignItems: "center",
              justifyContent: "center",
              borderRadius: 20,
              marginTop: 12,
              marginRight: 12,
              borderWidth: 1,
              borderColor: "white",
            }}
          >
            <Icon
              name="ios-log-out"
              size={24}
              style={{ marginLeft: 4 }}
              color={Colors.primary}
            />
          </View>
        </TouchableWithoutFeedback>
      </View>
      <View
        style={{
          width: metrics.screenWidth,
          height: metrics.screenHeight / 3 - 24,
          alignItems: "center",
          justifyContent: "flex-end",
        }}
      >
        <Image
          source={require("../../assets/img/user.png")}
          style={{
            width: 120,
            height: 120,
            borderRadius: 200,
            borderWidth: 3,
            borderColor: "white",
          }}
        />
      </View>
      <View>
        <Text>Name</Text>
        <TextInput
          style={{ backgroundColor: "pink", width: 100, height: 50 }}
        />
      </View>
    </Container>
  );
};

export default Profile;

const styles = StyleSheet.create({
  activityButton: {
    backgroundColor: colors.primary,
    borderRadius: 5,
    paddingHorizontal: 10,
    paddingVertical: 3,
  },
  activityTitle: { color: colors.white, fontSize: 15 },
});

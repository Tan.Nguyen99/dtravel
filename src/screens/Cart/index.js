import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  FlatList,
  StyleSheet,
} from "react-native";
import { Container, EmptyView, DTText } from "../../components";
import metrics from "../../themes/Metrics";
import Icons from "react-native-vector-icons/FontAwesome";
import Icon from "react-native-vector-icons/Ionicons";
import { Colors, Fonts, Metrics } from "../../themes";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import MapView, {
  PROVIDER_GOOGLE,
  Marker,
  Callout,
  Polygon,
  Polyline,
} from "react-native-maps";
import Geolocation from "@react-native-community/geolocation";
import RNAndroidLocationEnabler from "react-native-android-location-enabler";
import RNGooglePlaces from "react-native-google-places";
import { useSelector } from "react-redux";

const Cart = () => {
  const SITE = [];
  const currentLocationStore = useSelector(
    (state) => state.app.currentLocation
  );
  useEffect(() => {
    setCurrentLocation(currentLocationStore);
  }, []);

  const [currentLocation, setCurrentLocation] = useState({
    latitude: 37.4219857,
    latitudeDelta: 0.015,
    longitude: -122.0840379,
    longitudeDelta: 0.0121,
  });

  const onSearchLocation = () => {
    RNGooglePlaces.openAutocompleteModal(
      {
        initialQuery: "",
        latitude: currentLocation.latitude,
        longitude: currentLocation.longitude,
        radius: 10,
        country: "VN",
        types: ["establishment", "address"],
      },
      [
        "placeID",
        "location",
        "name",
        "address",
        "types",
        "openingHours",
        "plusCode",
        "rating",
        "userRatingsTotal",
        "viewport",
      ]
    )
      .then((place) => {
        console.log(place);
      })
      .catch((error) => console.log(error.message));
  };

  const renderItem = (item) => {
    return (
      <View
        style={{
          width: metrics.screenWidth - 24,
          height: metrics.screenHeight / 5,
          marginBottom: 12,
          borderRadius: 12,
          flexDirection: "row",
          borderWidth: 1,
          borderColor: Colors.divider,
          backgroundColor: "white",
        }}
      >
        <Image
          source={require("../../assets/img/site1.jpg")}
          style={{
            width: (metrics.screenWidth * 2) / 5,
            height: metrics.screenHeight / 5,
            borderRadius: 12,
            alignSelf: "center",
          }}
        />
        <View style={{ flex: 1 }}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "flex-end",
            }}
          >
            <TouchableWithoutFeedback
              onPress={() => {
                onSearchLocation();
              }}
            >
              <View
                style={{
                  backgroundColor: Colors.error,
                  width: 28,
                  height: 28,
                  alignItems: "center",
                  justifyContent: "center",
                  borderRadius: 12,
                }}
              >
                <Icon name="md-close" size={24} color="white" />
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      </View>
    );
  };

  const renderEmptyView = () => {
    // const { loading } = this.props;
    return <EmptyView title={"Empty"} />;
  };
  return (
    <Container style={{ alignItems: "center", flex: 1, marginTop: 55 }}>
      <View
        style={{
          width: Metrics.screenWidth - 24,
          flex: 1,
        }}
      >
        <View style={{ flex: 1 }}>
          {/* <DateTimePickerModal isVisible={true} mode="date" /> */}
          <View style={{}}>
            <DTText type="h3" marginBottom={12}>
              Catching Place
            </DTText>
            <View style={{ flexDirection: "row" }}>
              <MapView
                initialRegion={currentLocation}
                style={{ width: 180, height: 120 }}
              >
                <Marker
                  coordinate={{
                    latitude: currentLocation.latitude,
                    longitude: currentLocation.longitude,
                  }}
                ></Marker>
              </MapView>
              <Icon name="ios-compass" size={28} color={Colors.primary} />
              <DTText>101B Le Huu Trac</DTText>
            </View>
          </View>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={SITE}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            ListEmptyComponent={renderEmptyView}
          />
        </View>
        <TouchableWithoutFeedback>
          <View
            style={{
              width: metrics.screenWidth - 24,
              height: 48,
              backgroundColor: "#51cb96",
              borderRadius: 5,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text
              style={{
                color: Colors.white,
                fontWeight: Fonts.fontWeight.medium,
                fontSize: Fonts.fontSize.xMedium,
              }}
            >
              Book Tour
            </Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    </Container>
  );
};

export default Cart;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    // backgroundColor: "green",
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

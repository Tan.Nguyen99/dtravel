import Immutable from "seamless-immutable";
import { AppTypes } from "./actions";
import { makeReducerCreator } from "../../utils/reduxUtils";

export const INITIAL_STATE = Immutable({
  networkStatus: null,
  currentLocation: null,
});

export const changeNetworkStatus = (state, { status }) =>
  state.merge({ networkStatus: status });
export const getCurrentLocation = (state, { location }) =>
  state.merge({ currentLocation: location });

const ACTION_HANDLERS = {
  [AppTypes.CHANGE_NETWORK_STATUS]: changeNetworkStatus,
  [AppTypes.GET_CURRENT_LOCATION]: getCurrentLocation,
};

export default makeReducerCreator(INITIAL_STATE, ACTION_HANDLERS);

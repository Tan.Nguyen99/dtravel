import { makeActionCreator, makeConstantCreator } from "../../utils/reduxUtils";

export const AppTypes = makeConstantCreator(
  "STARTUP",
  "CHANGE_NETWORK_STATUS",
  "GET_CURRENT_LOCATION"
);

const startup = () => makeActionCreator(AppTypes.STARTUP);

const changeNetworkStatus = (status) =>
  makeActionCreator(AppTypes.CHANGE_NETWORK_STATUS, { status });

const getCurrentLocation = (location) =>
  makeActionCreator(AppTypes.GET_CURRENT_LOCATION, { location });

export default {
  startup,
  changeNetworkStatus,
  getCurrentLocation,
};
